import Realm from 'realm'

const AuthSchemaObject = {
  name: 'Auth',
  primaryKey: 'id',
  properties: {
    id: { type: 'string', default: '' },
    access_token: { type: 'string', default: '' },
    logged_user: { type: 'int', default: '' },
    user: { type: 'string', default: '' },
  },
}

class AuthSchema extends Realm.Object {}
AuthSchema.schema = AuthSchemaObject

export default new Realm({ schema: [AuthSchema] })
