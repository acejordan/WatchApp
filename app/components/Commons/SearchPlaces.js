import React, { Component } from 'react'
import { Text, ActivityIndicator, StatusBar, Dimensions, View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'

import {
  Container,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Header,
  Footer,
  Left,
  Right,
  Icon,
  Body
} from 'native-base'
import { Toast, Modal, Card } from 'antd-mobile'
import { NavigationActions } from '../../utils'
import { computeSize } from '../../utils/DeviceRatio'
import { resetNavigateTo, backAction } from '../../components/Commons/CustomRouteActions'
import { Layout } from '../../components'
import MapScreen from '../../containers/MapScreen'
import { createForm } from 'rc-form'

const { height, width } = Dimensions.get('window')

class SearchPlaces extends Component {
  static navigationOptions = {
    title: 'Login',
  }

  onWrapTouchStart = (e) => {
    if (!/iPhone|iPod|iPad/i.test(navigator.userAgent)) {
      return;
    }
    const pNode = closest(e.target, '.am-modal-content');
    if (!pNode) {
      e.preventDefault();
    }
  }

  render() {
    return (
      <Layout>

      </Layout>
    )
  }
}

export default SearchPlaces
