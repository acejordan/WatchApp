import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { Text, Container, Icon, Content, Button, Picker,Form, Tab, Tabs, Left, Right, Thumbnail, Body } from 'native-base'
import { Modal, Toast, List, Radio } from 'antd-mobile'
import { NavigationActions } from '../utils'
import { CustomCard, Layout } from '../components'
import { navigateTo } from '../components/Commons/CustomRouteActions'
import { computeSize } from '../utils/DeviceRatio'
import MapScreen from '../containers/MapScreen'
import moment from 'moment'
import OneSignal from 'react-native-onesignal'

const RadioItem = Radio.RadioItem

@connect(({ app }) => ({ ...app }))
class EventDetails extends Component {
  state = {
    locValue: 'home',
    transpoValue: 'bike',
    transpoLabel: 'Bike',
    evLoc: JSON.parse(this.props.navigation.state.params.event_location_details),
    homeLoc: JSON.parse(this.props.navigation.state.params.user.home_location_details),
    eventID: this.props.navigation.state.params.id
  }
  componentWillMount() {
    //OneSignal.init("e2c3b80e-34fb-4910-8582-4149fa8b73a2");
  
    //OneSignal.addEventListener('received', this.onReceived);
    //OneSignal.addEventListener('opened', this.onOpened);
    //OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
      //OneSignal.removeEventListener('received', this.onReceived);
      //OneSignal.removeEventListener('opened', this.onOpened);
      //OneSignal.removeEventListener('ids', this.onIds);
  }
  onReceived(notification) {
    //console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    //console.log('Message: ', openResult.notification.payload.body);
    //console.log('Data: ', openResult.notification.payload.additionalData);
    //console.log('isActive: ', openResult.notification.isAppInFocus);
    //console.log('openResult: ', openResult);
  }

  onIds(device) {
    //console.log('Device info: ', device);
  }

  componentDidMount () {
    let {params} = this.props.navigation.state

    this.setState({
      locValue: params.return_plan,
      transpoValue: params.return_transpo
    })
  }

  onSubmit = () => {
    Modal.alert('Confirmation', 'Save event details?', [
      { text: 'Cancel', onPress: () => console.log('cancel') },
      {
        text: 'Ok',
        onPress: () => {
          this.onConfirm()
          Toast.success('Event successfully added', 1.5)
          navigateTo(this.props.navigation, 'Home')
        },
      },
    ])
  }

  onConfirm = () => {
    let payload = {
      event_id: this.props.navigation.state.params.id,
      return_plan: this.state.locValue,
      return_transpo: this.state.transpoValue
    }

    this.props.dispatch({
      type: 'events/updateEventPlan',
      payload,
      callback: this.onSuccess
    })
  }

  locChange = (locValue) => {
    let { params } = this.props.navigation.state
    let homeLoc = JSON.parse(params.user.home_location_details)
    
    if(locValue === 'saved')
      homeLoc = JSON.parse(params.user.saved_location_details)

    this.setState({
      locValue,
      evLoc: JSON.parse(params.event_location_details),
      homeLoc
    })
  }

  transpoChange = (transpoValue, transpoLabel) => {
    this.setState({
      transpoValue,
      transpoLabel
    })
  }

  mapFullView = () => {
    let { params } = this.props.navigation.state
    let address = JSON.parse(params.event_location_details)
    navigateTo(this.props.navigation, 'MapScreenFullView', { address, event: params })
  }

  render() {
    let {params} = this.props.navigation.state
    
    const savedLocations = [
      { value: 'home', label: 'Home' },
      { value: 'saved', label: 'Saved Location' }
    ]
    const homeTranspo = [
      { value: 'walk', label: 'Walk' },
      { value: 'bus', label: 'Bus' },
      { value: 'train', label: 'Train' },
      { value: 'bike', label: 'Bike'},
      { value: 'taxi', label: 'Lyft/Taxi'},
      { value: 'vehicle', label: 'Personal Vehicle'}
    ]

    let address = JSON.parse(params.event_location_details)

    return (
      <Layout 
        noBackground bottomButton={this.mapFullView} 
        bottomButtonText="Full Map View Mode"
      >

        <View style={{ marginTop: 30 }}>
          <Text
            style={{
              fontSize: computeSize(60),
              fontFamily: 'BentonSans Regular',
              color: '#454545',
            }}
          >
            {params.title}
          </Text>
        </View>
        
        <View style={{ marginTop: 10, marginBottom: 20 }}>
          <Text
            style={{
              fontSize: computeSize(35),
              fontFamily: 'BentonSans Regular',
              color: 'gray',
            }}
          >
            by {params.user.fullname}
          </Text>
        </View>
        
        
        <View>
          <Text style={{ fontFamily: 'BentonSans Thin', color: '#454545' }}>Event Details</Text>
        </View>

        <View  style={{ 
          borderBottomWidth: 0, 
          marginLeft: 0, 
          backgroundColor: 'rgba(255,255,255,0.4)', 
          borderRadius: 5,
          marginBottom: 5,
          flexDirection: 'row',
          alignContent: 'flex-start',
          marginTop: 10
          }} >

          <Icon type="Feather" name="calendar" size={20} style={{ color: '#2d2d2d' }} />

          <Body style={{ alignItems: 'flex-start', marginLeft: 5 }}>
            <Text
              style={{
                fontSize: computeSize(35),
                fontFamily: 'BentonSans Regular',
                color: '#454545',
              }}
            >
              {moment(params.event_date + ' ' + params.event_time).format('MMMM Do, hh:mm A')}
            </Text>
          </Body>
        </View>

        <View  style={{ 
          borderBottomWidth: 0, 
          marginLeft: 0, 
          backgroundColor: 'rgba(255,255,255,0.4)', 
          borderRadius: 5,
          marginBottom: 5,
          flexDirection: 'row',
          alignContent: 'flex-start',
          marginTop: 20,
          marginBottom: 20,
          }} >

          <Icon type="Feather" name="map-pin" size={20} style={{ color: '#2d2d2d' }} />

          <Body style={{ alignItems: 'flex-start', marginLeft: 5 }}>
            <Text
              style={{
                fontSize: computeSize(35),
                fontFamily: 'BentonSans Regular',
                color: '#454545',
                marginBottom: 5
              }}
            >
              {address.name}
            </Text>
            <Text
              style={{
                fontSize: computeSize(35),
                fontFamily: 'BentonSans Regular',
                color: 'gray',
              }}
            >
              {address.addressComponents.locality}, {address.addressComponents.administrative_area_level_2}
            </Text>
          </Body>
        </View>
              
        <View>
          <Text style={{ fontFamily: 'BentonSans Thin', color: '#454545' }}>Details after the event</Text>
        </View>

        <View  style={{ 
          borderBottomWidth: 0, 
          marginLeft: 0, 
          backgroundColor: 'rgba(255,255,255,0.4)', 
          borderRadius: 5,
          marginBottom: 5,
          flexDirection: 'row',
          alignContent: 'flex-start',
          marginTop: 10
          }} >

          <Icon type="Feather" name="map-pin" size={20} style={{ color: '#2d2d2d' }} />

          <Body style={{ alignItems: 'flex-start', marginLeft: 5 }}>
            <Text
              style={{
                fontSize: computeSize(35),
                fontFamily: 'BentonSans Regular',
                color: '#454545',
                marginBottom: 5
              }}
            >
              {address.name}
            </Text>
            <Text
              style={{
                fontSize: computeSize(35),
                fontFamily: 'BentonSans Regular',
                color: 'gray',
              }}
            >
              {address.addressComponents.locality}, {address.addressComponents.administrative_area_level_2}
            </Text>
          </Body>
        </View>
        
        <View  style={{ 
          borderBottomWidth: 0, 
          marginLeft: 0, 
          backgroundColor: 'rgba(255,255,255,0.4)', 
          borderRadius: 5,
          marginBottom: 5,
          flexDirection: 'row',
          alignContent: 'flex-start',
          marginTop: 10,
          marginBottom: 20
          }} >

          <Icon type="Feather" name="rotate-ccw" size={20} style={{ color: '#2d2d2d' }} />

          <Body style={{ alignItems: 'flex-start', marginLeft: 5 }}>
            <Text
              style={{
                fontSize: computeSize(35),
                fontFamily: 'BentonSans Regular',
                color: '#454545',
                marginBottom: 5
              }}
            >
              Return Transit
            </Text>
            <Text
              style={{
                fontSize: computeSize(35),
                fontFamily: 'BentonSans Regular',
                color: 'gray',
              }}
            >
              {this.state.transpoLabel}
            </Text>
          </Body>
        </View>
        
        <View>
          <Text style={{ fontFamily: 'BentonSans Thin', color: '#454545' }}>Map View</Text>
        </View>
        
        <View style={{ flex: 1, height: 300, marginTop: 10 }}>

          <MapScreen 
              style={{ position: 'relative', height: '100%' }}
              eventLocation={this.state.evLoc}
              homeLocation={this.state.homeLoc}
              transit={this.state.transpoLabel}
            />

        </View>

        {/* <CustomCard>
          <Form>
            <View
              style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'space-between'
              }}>
              <View style={{ flex: 1, flexDirection: 'column' }}>
                <Tabs initialPage={0}>
                  <Tab heading="Details">
                    <List renderHeader={() => 'Location after event?'}>
                      {savedLocations.map(i => (
                        <View>
                          <Left>
                            <Text>Lunch Break</Text>
                          </Left>
                          <Right>
                            <Radio
                            color={"#f0ad4e"}
                            selectedColor={"#5cb85c"}
                            selected={false}
                            />
                          </Right>
                        </View>
                      ))}
                    </List>

                    <List renderHeader={() => 'How will you get to location?'}>
                      {homeTranspo.map(i => (
                        <RadioItem key={i.value} checked={this.state.transpoValue === i.value} onChange={() => this.transpoChange(i.value, i.label)}>
                          {i.label}
                        </RadioItem>
                      ))}
                    </List>
                  </Tab>
                  <Tab heading="Map View">

                  </Tab>
                </Tabs>

              </View>
            </View>
          </Form>
        </CustomCard> */}

      </Layout>
    )
  }
}

export default EventDetails
